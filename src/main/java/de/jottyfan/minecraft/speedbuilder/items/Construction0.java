package de.jottyfan.minecraft.speedbuilder.items;

import de.jottyfan.minecraft.speedbuilder.SpeedBuilder;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLLog;
import scala.swing.TextComponent;

/**
 * 
 * @author jotty
 *
 */
public class Construction0 extends Item {

	public Construction0(CreativeTabs tabs) {
		super();
		super.setRegistryName(SpeedBuilder.MODID, "construction0");
		super.setUnlocalizedName("construction0");
		super.setCreativeTab(tabs);
		super.setMaxStackSize(1);
	}

	@Override
	public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand,
			EnumFacing facing, float hitX, float hitY, float hitZ) {
		String x = new Integer(pos.getX()).toString();
		String y = new Integer(pos.getY()).toString();
		String z = new Integer(pos.getZ()).toString();
		ITextComponent msg = new TextComponentTranslation("msg.buildingplan.start", x, y, z);
		if (worldIn.isRemote) {
			player.sendMessage(msg);
		}
		EnumActionResult result = super.onItemUse(player, worldIn, pos, hand, facing, hitX, hitY, hitZ);
		// building plans are not stackable, so we need not dropping them here
		Construction1 cons1 = (Construction1) Item.getByNameOrId("speedbuilder:construction1");
		if (cons1 == null) {
			FMLLog.bigWarning("could not find item speedbuilder:construction1");
		} else {
			ItemStack newStack = new ItemStack(cons1);
			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setLong("startingPos", pos.toLong());
			newStack.setTagCompound(nbt);
			player.setHeldItem(hand, newStack);
		}
		return result;
	}
}
