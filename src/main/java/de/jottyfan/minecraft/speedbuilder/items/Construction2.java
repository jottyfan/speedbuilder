package de.jottyfan.minecraft.speedbuilder.items;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.SerializationUtils;

import de.jottyfan.minecraft.speedbuilder.SpeedBuilder;
import de.jottyfan.minecraft.speedbuilder.util.SpeedBuilderBlocks;
import net.minecraft.block.Block;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLLog;
import scala.actors.threadpool.Arrays;

/**
 * 
 * @author jotty
 *
 */
public class Construction2 extends Item {

	public Construction2(CreativeTabs tabs) {
		super();
		super.setRegistryName(SpeedBuilder.MODID, "construction2");
		super.setUnlocalizedName("construction2");
		super.setCreativeTab(tabs);
		super.setMaxStackSize(1);
	}

	public ConstructionBean getConstructionBeanFromNbt(EntityPlayer player, ItemStack stack) {
		NBTTagCompound nbt = stack.getTagCompound();
		byte[] byteArray = nbt != null ? nbt.getByteArray("constructionBean") : null;
		return byteArray == null ? null : ConstructionBean.deserialize(player, byteArray);
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
		ConstructionBean bean = getConstructionBeanFromNbt(null, stack);
		if (bean != null && tooltip != null) {
			tooltip.add(bean.toSummary());
		}
	}

	@Override
	public EnumActionResult onItemUse(EntityPlayer player, World world, BlockPos pos, EnumHand hand, EnumFacing facing,
			float hitX, float hitY, float hitZ) {
		ItemStack stack = player.getHeldItem(hand);
		ConstructionBean bean = getConstructionBeanFromNbt(player, stack);
		if (bean == null) {
			ITextComponent msg = new TextComponentTranslation("msg.buildingplan.null");
			if (world.isRemote) {
				player.sendMessage(msg);
			}
		} else if (player.isSneaking()) {
			bean.dropBlocksOntoGround(world, pos);
		} else {
			String found = world.getBlockState(pos).getBlock().getUnlocalizedName();
			if (found.equals(SpeedBuilderBlocks.MIRRORDIAGONAL.getUnlocalizedName())) {
				bean.mirrorDiagonal(player, world);
			} else if (found.equals(SpeedBuilderBlocks.MIRRORHORIZONTAL.getUnlocalizedName())) {
				bean.mirrorHorizontal(player, world);
			} else if (found.equals(SpeedBuilderBlocks.MIRRORVERTICAL.getUnlocalizedName())) {
				bean.mirrorVertical(player, world);
			} else {
				bean.fillBlocksFromInventory(player);
				Map<Block, Integer> map = bean.getAllMissingBlocks();
				if (map.size() < 1) {
					bean.build(world, pos.up());
				} else {
					StringBuilder buf = new StringBuilder();
					for (Entry<Block, Integer> entry : map.entrySet()) {
						buf.append(entry.getValue()).append("x ");
						buf.append(entry.getKey().getLocalizedName()).append(", ");
					}
					String mes = buf.toString();
					if (mes.length() > 1) { // remove the trailing ,
						mes = mes.substring(0, mes.length() - 2);
					}
					ITextComponent msg = new TextComponentTranslation("msg.buildingplan.missing", mes);
					if (world.isRemote) {
						player.sendMessage(msg);
					}
				}
			}
		}
		byte[] constructionBeanByteArray = bean.serialize();
		NBTTagCompound nbt = stack.getTagCompound();
		nbt.setByteArray("constructionBean", constructionBeanByteArray);
		return super.onItemUse(player, world, pos, hand, facing, hitX, hitY, hitZ);
	}
}
