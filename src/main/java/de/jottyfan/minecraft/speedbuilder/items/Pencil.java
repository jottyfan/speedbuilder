package de.jottyfan.minecraft.speedbuilder.items;

import de.jottyfan.minecraft.speedbuilder.SpeedBuilder;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLLog;
import scala.swing.TextComponent;

/**
 * 
 * @author jotty
 *
 */
public class Pencil extends Item {

	public Pencil(CreativeTabs tabs) {
		super();
		super.setRegistryName(SpeedBuilder.MODID, "pencil");
		super.setUnlocalizedName("pencil");
		super.setCreativeTab(tabs);
	}
}
