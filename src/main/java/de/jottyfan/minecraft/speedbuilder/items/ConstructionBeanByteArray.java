package de.jottyfan.minecraft.speedbuilder.items;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import net.minecraftforge.fml.common.FMLLog;

/**
 * 
 * @author jotty
 *
 */
public class ConstructionBeanByteArray {

	private List<byte[]> list;

	public ConstructionBeanByteArray() {
		super();
		list = new ArrayList<byte[]>();
	}

	public ConstructionBeanByteArray(byte[] array) {
		super();
		list = new ArrayList<byte[]>();
		for (int i = 0; i < (array.length / ConstructionBlockSerializer.ARRAY_SIZE); i++) {
			ByteBuffer a = ByteBuffer.allocate(ConstructionBlockSerializer.ARRAY_SIZE);
			try {
				a.put(array, i * ConstructionBlockSerializer.ARRAY_SIZE, ConstructionBlockSerializer.ARRAY_SIZE);
			} catch (BufferUnderflowException e) {
				StringBuilder buf = new StringBuilder("a.position = ");
				buf.append(a.position()).append("/").append(a.limit());
				buf.append(", array.position = ");
				buf.append(i * ConstructionBlockSerializer.ARRAY_SIZE).append("/").append(array.length);
				FMLLog.bigWarning(buf.toString());
			}
			list.add(a.array());
		}
	}

	public void add(byte[] array) {
		list.add(array);
	}

	public List<byte[]> getList() {
		return list;
	}

	public byte[] getByteArray() {
		ByteBuffer b = ByteBuffer.allocate(ConstructionBlockSerializer.ARRAY_SIZE * list.size());
		for (byte[] elem : list) {
			b.put(elem);
		}
		return b.array();
	}
}
