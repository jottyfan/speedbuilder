package de.jottyfan.minecraft.speedbuilder.items;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.SerializationUtils;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLLog;

/**
 * 
 * @author jotty
 *
 */
public class ConstructionBean {
	private final List<ConstructionBlock> list;

	public ConstructionBean(World world, BlockPos start, BlockPos end) {
		super();
		this.list = new ArrayList<>();
		createConstructionPlan(world, start, end);
	}

	private ConstructionBean(List<ConstructionBlock> list) {
		super();
		this.list = list;
	}

	public String toString() {
		StringBuilder buf = new StringBuilder("ConstructionBean{");
		for (ConstructionBlock block : list) {
			buf.append(block);
		}
		buf.append("}");
		return buf.toString();
	}

	public Integer getMaxX() {
		int maxx = 0;
		for (ConstructionBlock block : list) {
			maxx = block.getPos().getX() > maxx ? block.getPos().getX() : maxx;
		}
		return 0;
	}

	public Integer getMaxZ() {
		int maxz = 0;
		for (ConstructionBlock block : list) {
			maxz = block.getPos().getZ() > maxz ? block.getPos().getZ() : maxz;
		}
		return 0;
	}

	/**
	 * build the building on the building plan at pos
	 * 
	 * @param worldIn the world
	 * @param pos     the position
	 */
	public void build(World worldIn, BlockPos pos) {
		for (ConstructionBlock cBlock : list) {
			BlockPos buildPos = pos.add(cBlock.getPos());
			boolean built = buildBlock(worldIn, buildPos, cBlock.getBlockState());
			if (built) {
				cBlock.setSet(false);
			}
		}
	}

	/**
	 * build a block at pos
	 * 
	 * @param worldIn
	 * @param pos
	 * @param blockState
	 */
	private boolean buildBlock(World worldIn, BlockPos pos, IBlockState blockState) {
		IBlockState oldState = worldIn.getBlockState(pos);
		worldIn.destroyBlock(pos, true);
		boolean done = worldIn.setBlockState(pos, blockState);
		if (done) {
			worldIn.notifyBlockUpdate(pos, oldState, blockState, 3); // flags as in setBlockState(BlockPos, IBlockState)
		}
		return done;
	}

	/**
	 * create the construction plan from the world
	 * 
	 * @param world the world
	 * @param start the starting position
	 * @param end   the ending position
	 */
	private void createConstructionPlan(World world, BlockPos start, BlockPos end) {
		Integer x1 = start.getX() > end.getX() ? end.getX() : start.getX();
		Integer x2 = start.getX() > end.getX() ? start.getX() : end.getX();
		Integer y1 = start.getY() > end.getY() ? end.getY() : start.getY();
		Integer y2 = start.getY() > end.getY() ? start.getY() : end.getY();
		Integer z1 = start.getZ() > end.getZ() ? end.getZ() : start.getZ();
		Integer z2 = start.getZ() > end.getZ() ? start.getZ() : end.getZ();
		for (int x = x1; x <= x2; x++) {
			for (int y = y1; y <= y2; y++) {
				for (int z = z1; z <= z2; z++) {
					BlockPos pos = new BlockPos(x, y, z);
					IBlockState blockState = world.getBlockState(pos);
					String blockName = blockState.getBlock().getRegistryName().toString();
					Boolean ignore = blockName.equals("minecraft:air");
					ignore = ignore || blockName.equals("minecraft:water");
					ignore = ignore || blockName.equals("minecraft:lava");
					ignore = ignore || blockName.equals("speedbuilder:constructionborder");
					if (!ignore) {
						Integer xDiff = x - x1;
						Integer yDiff = y - y1;
						Integer zDiff = z - z1;
						BlockPos newPos = new BlockPos(xDiff, yDiff, zDiff);
						blockState = replaceBlockState(blockState);
						list.add(new ConstructionBlock(newPos, blockState));
					}
				}
			}
		}
	}

	/**
	 * replace some blocks
	 * 
	 * @param blockState block to be checked
	 * @return block itself or replacement
	 */
	private IBlockState replaceBlockState(IBlockState blockState) {
		String blockName = blockState.getBlock().getRegistryName().toString();
		if (blockName.equals("minecraft:grass")) {
			return Block.getBlockFromName("minecraft:dirt").getDefaultState();
		} else if (blockName.equals("minecraft:bedrock")) {
			return Block.getBlockFromName("minecraft:stone").getDefaultState();
		}
		return blockState;
	}

	/**
	 * add block requirement to the building plan
	 * 
	 * @param blockState the needed block
	 * @param pos        the position
	 */
	public void addBlock(IBlockState blockState, BlockPos pos) {
		list.add(new ConstructionBlock(pos, blockState));
	}

	/**
	 * fill one block if required and return true, if not required, return false
	 * 
	 * @param item the item
	 * @return true or false
	 */
	private Boolean fillOneBlock(Item item) {
		for (ConstructionBlock cBlock : list) {
			if (!cBlock.isSet()) {
				Block block = Block.getBlockFromItem(item);
				boolean found = cBlock.getBlockState().getBlock().getRegistryName().equals(block.getRegistryName());
				if (found) {
					cBlock.setSet(true);
					// abort to speed up
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * fill the block requirements by this item stack's elements
	 * 
	 * @param itemStack the itemStack
	 */
	private void fulfillBlockRequirement(ItemStack itemStack) {
		Integer used = 0;
		for (Integer i = 0; i < itemStack.getCount(); i++) {
			used += fillOneBlock(itemStack.getItem()) ? 1 : 0;
		}
		itemStack.setCount(itemStack.getCount() - used);
	}

	/**
	 * get a list of blocks that are not filled in yet (left over building plan
	 * requirements)
	 * 
	 * @return a list of blocks (an empty one at least)
	 */
	public Map<Block, Integer> getAllMissingBlocks() {
		Map<Block, Integer> map = new HashMap<>();
		for (ConstructionBlock cBlock : list) {
			if (!cBlock.isSet()) {
				Integer amount = map.get(cBlock.getBlockState().getBlock());
				if (amount == null) {
					amount = 0;
				}
				amount++;
				map.put(cBlock.getBlockState().getBlock(), amount);
			}
		}
		return map;
	}

	public List<ConstructionBlock> getList() {
		return list;
	}

	/**
	 * fill blocks in the building plan from the user's inventory
	 * 
	 * @param player the player with an inventory
	 */
	public void fillBlocksFromInventory(EntityPlayer player) {
		Container c = player.inventoryContainer;
		List<Slot> slots = c.inventorySlots;
		for (Slot slot : slots) {
			fulfillBlockRequirement(slot.getStack());
		}
	}

	/**
	 * do only serialize what is needed here
	 * 
	 * @return the serialized byte array
	 */
	public byte[] serialize() {
		ConstructionBeanByteArray cbba = new ConstructionBeanByteArray();
		for (ConstructionBlock block : list) {
			cbba.add(block.toByteArray());
		}
		return cbba.getByteArray();
	}

	/**
	 * do only deserialize what is needed here
	 * 
	 * @param nbt the byte array
	 * @return
	 */
	public static final ConstructionBean deserialize(EntityPlayer player, byte[] nbt) {
		ConstructionBeanByteArray cbba = new ConstructionBeanByteArray(nbt);
		List<ConstructionBlock> list = new ArrayList<>();
		for (byte[] elem : cbba.getList()) {
			ConstructionBlock block = ConstructionBlock.fromByteArray(player, elem);
			if (block != null) {
				list.add(block);
			}
		}
		return new ConstructionBean(list);
	}

	/**
	 * create a short summary about this building plan
	 * 
	 * @return the summary
	 */
	public String toSummary() {
		StringBuilder buf = new StringBuilder();
		buf.append(list.size()).append(" blocks");
		return buf.toString();
	}

	/**
	 * switch all x and z coordinates of list
	 */
	public void mirrorDiagonal(EntityPlayer player, World world) {
		for (ConstructionBlock block : list) {
			block.resetBlockPos(block.getPos().getZ(), block.getPos().getY(), block.getPos().getX());
		}
		if (world.isRemote) {
			player.sendMessage(new TextComponentTranslation("msg.buildingplan.mirrordiagonal"));
		}
	}

	/**
	 * reverse x
	 */
	public void mirrorHorizontal(EntityPlayer player, World world) {
		int maxx = getMaxX();
		for (ConstructionBlock block : list) {
			block.resetBlockPos(maxx - block.getPos().getX(), block.getPos().getY(), block.getPos().getZ());
		}
		ITextComponent msg = new TextComponentTranslation("msg.buildingplan.mirrorhorizontal");
		if (world.isRemote) {
			player.sendMessage(msg);
		}
	}

	/**
	 * reverse z
	 */
	public void mirrorVertical(EntityPlayer player, World world) {
		int maxz = getMaxZ();
		for (ConstructionBlock block : list) {
			block.resetBlockPos(block.getPos().getX(), block.getPos().getY(), maxz - block.getPos().getZ());
		}
		ITextComponent msg = new TextComponentTranslation("msg.buildingplan.mirrorvertical");
		if (world.isRemote) {
			player.sendMessage(msg);
		}
	}

	/**
	 * drop all blocks in building plan
	 * 
	 * @param world the world
	 * @param pos   the position to drop it to
	 */
	public void dropBlocksOntoGround(World world, BlockPos pos) {
		for (ConstructionBlock c : list) {
			if (c.isSet()) {
				Block block = c.getBlockState().getBlock();
				Item item = block.getItemDropped(c.getBlockState(), new Random(), 1);
				ItemStack stack = new ItemStack(item);
				EntityItem entity = new EntityItem(world, pos.getX(), pos.getY(), pos.getZ(), stack);
				world.spawnEntity(entity);
				c.setSet(false);
			}
		}
	}

}
