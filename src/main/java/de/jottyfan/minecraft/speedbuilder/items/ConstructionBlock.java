package de.jottyfan.minecraft.speedbuilder.items;

import java.io.Serializable;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.FMLLog;

/**
 * 
 * @author jotty
 *
 */
public class ConstructionBlock implements Serializable {
	private BlockPos pos;
	private final IBlockState blockState;
	private boolean isSet;

	public ConstructionBlock(BlockPos pos, IBlockState blockState) {
		super();
		this.pos = pos;
		this.blockState = blockState;
		this.isSet = false;
	}

	public String toString() {
		StringBuilder buf = new StringBuilder("ConstructionBlock@");
		buf.append(pos).append("{");
		buf.append(blockState);
		buf.append("}");
		return buf.toString();
	}
	
	public byte[] toByteArray() {
		return new ConstructionBlockSerializer().getBytes(pos, blockState, isSet);
	}

	public static final ConstructionBlock fromByteArray(EntityPlayer player, byte[] array) {
		return new ConstructionBlockSerializer().getConstructionBlock(player, array);
	}

	public void resetBlockPos(int x, int y, int z) {
		pos = new BlockPos(x, y, z);
	}

	public BlockPos getPos() {
		return pos;
	}

	public IBlockState getBlockState() {
		return blockState;
	}

	public boolean isSet() {
		return isSet;
	}

	public void setSet(boolean isSet) {
		this.isSet = isSet;
	}
}
