package de.jottyfan.minecraft.speedbuilder.items;

import java.util.Arrays;

import de.jottyfan.minecraft.speedbuilder.SpeedBuilder;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLLog;

/**
 * 
 * @author jotty
 *
 */
public class Construction1 extends Item {

	public Construction1(CreativeTabs tabs) {
		super();
		super.setRegistryName(SpeedBuilder.MODID, "construction1");
		super.setUnlocalizedName("construction1");
		super.setCreativeTab(tabs);
		super.setMaxStackSize(1);
	}

	public BlockPos getStartingPosFromNbt(ItemStack stack) {
		NBTTagCompound nbt = stack.getTagCompound();
		return nbt == null ? null : BlockPos.fromLong(nbt.getLong("startingPos"));
	}

	@Override
	public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand,
			EnumFacing facing, float hitX, float hitY, float hitZ) {
		BlockPos startingPos = getStartingPosFromNbt(player.getHeldItem(hand));
		if (startingPos != null) {
			String x = new Integer(pos.getX()).toString();
			String y = new Integer(pos.getY()).toString();
			String z = new Integer(pos.getZ()).toString();
			ITextComponent msg = new TextComponentTranslation("msg.buildingplan.end", x, y, z);
			if (worldIn.isRemote) {
				player.sendMessage(msg);
			}
			EnumActionResult result = super.onItemUse(player, worldIn, pos, hand, facing, hitX, hitY, hitZ);
			// building plans are not stackable, so we need not dropping them here
			ConstructionBean cBean = new ConstructionBean(worldIn, startingPos, pos);
			Construction2 cons2 = (Construction2) Item.getByNameOrId("speedbuilder:construction2");
			if (cons2 == null) {
				FMLLog.bigWarning("could not find item speedbuilder:construction2");
			} else {
				ItemStack newStack = new ItemStack(cons2);
				byte[] constructionBeanByteArray = cBean.serialize();
				NBTTagCompound nbt = new NBTTagCompound();
				nbt.setByteArray("constructionBean", constructionBeanByteArray);
				newStack.setTagCompound(nbt);
				player.setHeldItem(hand, newStack);
			}
		} else {
			ITextComponent msg = new TextComponentTranslation("msg.buildingplan.null");
			if (worldIn.isRemote) {
				player.sendMessage(msg);
			}
		}
		return super.onItemUse(player, worldIn, pos, hand, facing, hitX, hitY, hitZ);
	}
}
