package de.jottyfan.minecraft.speedbuilder.items;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.List;

import com.google.common.collect.ImmutableMap;

import io.netty.buffer.ByteBuf;
import net.minecraft.block.Block;
import net.minecraft.block.material.EnumPushReaction;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLLog;

/**
 * 
 * @author jotty
 *
 */
public class ConstructionBlockSerializer {

	private final static Short VERSION = 1;
	public final static Integer ARRAY_SIZE = 20;

	/**
	 * convert block pos, state and isset to byte array
	 * 
	 * @param pos        the pos
	 * @param blockState the state
	 * @param isSet      the is set
	 * @return byte array
	 */
	public byte[] getBytes(BlockPos pos, IBlockState blockState, boolean isSet) {
		byte[] v = ByteBuffer.allocate(2).putShort(VERSION).array();
		byte[] x = ByteBuffer.allocate(4).putInt(pos.getX()).array();
		byte[] y = ByteBuffer.allocate(4).putInt(pos.getY()).array();
		byte[] z = ByteBuffer.allocate(4).putInt(pos.getZ()).array();
		byte[] i = ByteBuffer.allocate(2).putShort(isSet ? (short) 1 : (short) 0).array();
		Integer bs = Block.getStateId(blockState);
		byte[] s = ByteBuffer.allocate(4).putInt(bs).array();

		Integer bbl = v.length;
		bbl += x.length;
		bbl += y.length;
		bbl += z.length;
		bbl += i.length;
		bbl += s.length;

		ByteBuffer b = ByteBuffer.allocate(bbl);
		b.put(v);
		b.put(x);
		b.put(y);
		b.put(z);
		b.put(i);
		b.put(s);

		return b.array();
	}

	/**
	 * convert the byte array to a ConstructionBlock
	 * 
	 * @param player the player for messages
	 * @param array  the array
	 * @return null on wrong version or the ConstructionBlock
	 */
	public ConstructionBlock getConstructionBlock(EntityPlayer player, byte[] array) {
		ByteBuffer bb = ByteBuffer.allocate(array.length);
		bb.put(array);
		bb.position(0); // set position to 0 to start from beginning on reading
		if (bb.limit() < ARRAY_SIZE) {
			FMLLog.bigWarning("byte array limit is too small, should be " + ARRAY_SIZE + " but is " + bb.limit());
			return null;
		}
		try {
			short v = bb.getShort();

			if (v != VERSION) {
				if (player != null) {
					ITextComponent c = new TextComponentString(
							"using wrong version for this construction block, please reset building plan");
					player.sendMessage(c);
				}
				FMLLog.bigWarning("wrong version of ConstructionBlockSerializer, aborting...");
				return null;
			}

			int x = bb.getInt();
			int y = bb.getInt();
			int z = bb.getInt();
			short i = bb.getShort();
			IBlockState blockState = Block.getStateById(bb.getInt());

			BlockPos pos = new BlockPos(x, y, z);
			Boolean isSet = i == 1;

			ConstructionBlock block = new ConstructionBlock(pos, blockState);
			block.setSet(isSet);
			return block;
		} catch (BufferUnderflowException e) {
			e.printStackTrace();
			FMLLog.bigWarning("not enough bytes");
			return null;
		}
	}
}
