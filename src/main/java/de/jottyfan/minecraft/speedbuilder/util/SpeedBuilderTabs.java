package de.jottyfan.minecraft.speedbuilder.util;

import de.jottyfan.minecraft.speedbuilder.SpeedBuilder;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * 
 * @author jotty
 *
 */
public class SpeedBuilderTabs extends CreativeTabs {
	
	private final String modId;
	private final String iconname;
	
	public SpeedBuilderTabs(String modId, String iconname) {
		super(SpeedBuilder.MODID);
		this.modId = modId;
		this.iconname = iconname;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public ItemStack getTabIconItem() {
		StringBuilder buf = new StringBuilder(modId);
		buf.append(":").append(iconname);
		Item item = Item.getByNameOrId(buf.toString());
		return new ItemStack(item);
	}
}
