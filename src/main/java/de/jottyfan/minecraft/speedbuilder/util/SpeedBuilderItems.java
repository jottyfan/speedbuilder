package de.jottyfan.minecraft.speedbuilder.util;

import de.jottyfan.minecraft.speedbuilder.SpeedBuilder;
import de.jottyfan.minecraft.speedbuilder.items.Construction0;
import de.jottyfan.minecraft.speedbuilder.items.Construction1;
import de.jottyfan.minecraft.speedbuilder.items.Construction2;
import de.jottyfan.minecraft.speedbuilder.items.ConstructionBean;
import de.jottyfan.minecraft.speedbuilder.items.Pencil;
import de.jottyfan.minecraft.speedbuilder.proxy.IProxy;
import net.minecraft.item.Item;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry.ObjectHolder;
import net.minecraftforge.registries.IForgeRegistry;
import scala.collection.mutable.HashSet;

/**
 * 
 * @author jotty
 *
 */
@ObjectHolder(SpeedBuilder.MODID)
public class SpeedBuilderItems {
	private static final SpeedBuilderTabs tabs = new SpeedBuilderTabs(SpeedBuilder.MODID, "construction2");

	public static final Construction0 ITEM_CONSTRUCTION0 = new Construction0(tabs);
	public static final Construction1 ITEM_CONSTRUCTION1 = new Construction1(tabs);
	public static final Construction2 ITEM_CONSTRUCTION2 = new Construction2(tabs);
	public static final Pencil ITEM_PENCIL = new Pencil(tabs);

	@SidedProxy(clientSide = "de.jottyfan.minecraft.speedbuilder.proxy.ClientProxy", serverSide = "de.jottyfan.minecraft.speedbuilder.proxy.ServerProxy")
	public static IProxy proxy;

	@Mod.EventBusSubscriber(modid = SpeedBuilder.MODID)
	public static class RegistrationHandler {
		public static final HashSet<Item> ITEMS = new HashSet<>();

		@SubscribeEvent
		public static void registerItems(final RegistryEvent.Register<Item> event) {
			final Item[] items = { ITEM_CONSTRUCTION0, ITEM_CONSTRUCTION1, ITEM_CONSTRUCTION2, ITEM_PENCIL };

			final IForgeRegistry<Item> registry = event.getRegistry();

			for (final Item item : items) {
				registry.register(item);

				proxy.registerCustomModel(item, 0, item.getRegistryName());

				ITEMS.add(item);
			}
		}
	}

}
