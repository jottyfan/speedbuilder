package de.jottyfan.minecraft.speedbuilder.util;

import de.jottyfan.minecraft.speedbuilder.SpeedBuilder;
import de.jottyfan.minecraft.speedbuilder.blocks.ConstructionBorder;
import de.jottyfan.minecraft.speedbuilder.blocks.MirrorDiagonal;
import de.jottyfan.minecraft.speedbuilder.blocks.MirrorHorizontal;
import de.jottyfan.minecraft.speedbuilder.blocks.MirrorVertical;
import de.jottyfan.minecraft.speedbuilder.proxy.IProxy;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.common.registry.GameRegistry.ObjectHolder;
import net.minecraftforge.registries.IForgeRegistry;
import scala.collection.mutable.HashSet;

/**
 * 
 * @author jotty
 *
 */
@ObjectHolder(SpeedBuilder.MODID)
public class SpeedBuilderBlocks {
	private static final SpeedBuilderTabs tabs = new SpeedBuilderTabs(SpeedBuilder.MODID, "constructionborder");

	public static final ConstructionBorder CONSTRUCTIONBORDER = new ConstructionBorder(tabs);
	public static final MirrorDiagonal MIRRORDIAGONAL = new MirrorDiagonal(tabs);
	public static final MirrorHorizontal MIRRORHORIZONTAL = new MirrorHorizontal(tabs);
	public static final MirrorVertical MIRRORVERTICAL = new MirrorVertical(tabs);

	@SidedProxy(clientSide = "de.jottyfan.minecraft.speedbuilder.proxy.ClientProxy", serverSide = "de.jottyfan.minecraft.speedbuilder.proxy.ServerProxy")
	public static IProxy proxy;

	@Mod.EventBusSubscriber(modid = SpeedBuilder.MODID)
	public static class RegistrationHandler {
		public static final HashSet<Block> BLOCKS = new HashSet<>();

		@SubscribeEvent
		public static void registerBlocks(final RegistryEvent.Register<Block> event) {
			final Block[] blocks = { CONSTRUCTIONBORDER, MIRRORDIAGONAL, MIRRORHORIZONTAL, MIRRORVERTICAL };

			final IForgeRegistry<Block> registry = event.getRegistry();

			for (final Block block : blocks) {
				registry.register(block);

				ItemBlock itemBlock = new ItemBlock(block);
				itemBlock.setRegistryName(block.getRegistryName());
				ForgeRegistries.ITEMS.register(itemBlock);

				proxy.registerCustomModel(Item.getItemFromBlock(block), 0, block.getRegistryName());

				BLOCKS.add(block);
			}
		}
	}

}
