package de.jottyfan.minecraft.speedbuilder.blocks;

import java.util.Random;

import de.jottyfan.minecraft.speedbuilder.SpeedBuilder;
import de.jottyfan.minecraft.speedbuilder.util.SpeedBuilderItems;
import net.minecraft.block.BlockBookshelf;
import net.minecraft.block.BlockDirt;
import net.minecraft.block.BlockFence;
import net.minecraft.block.BlockGravel;
import net.minecraft.block.BlockOre;
import net.minecraft.block.BlockPlanks;
import net.minecraft.block.BlockSand;
import net.minecraft.block.BlockWoodSlab;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;

/**
 * 
 * @author jotty
 *
 */
public class MirrorHorizontal extends BlockBookshelf {

	public MirrorHorizontal(CreativeTabs tab) {
		super();
		super.setRegistryName(SpeedBuilder.MODID, "mirrorhorizontal");
		super.setUnlocalizedName("mirrorhorizontal");
		super.setHardness(0.1f);
		super.setCreativeTab(tab);
	}

    /**
     * Returns the quantity of items to drop on block destruction.
     */
    public int quantityDropped(Random random)
    {
        return 1;
    }

    @Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune) {
		return Item.getItemFromBlock(this);
	}
}
