package de.jottyfan.minecraft.speedbuilder;

import de.jottyfan.minecraft.speedbuilder.util.SpeedBuilderBlocks;
import de.jottyfan.minecraft.speedbuilder.util.SpeedBuilderItems;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * 
 * @author jotty
 *
 */
public class Register {
	public void registerRecipes() {
		registerShapelessRecipes();
		registerShapedRecipes();
	}

	private void registerShapelessRecipes() {
		GameRegistry.addShapelessRecipe(new ResourceLocation("speedbuilder:pencil"), null,
				new ItemStack(SpeedBuilderItems.ITEM_PENCIL, 4), Ingredient.fromItem(Items.STICK),
				Ingredient.fromItem(Items.COAL));
		GameRegistry.addShapelessRecipe(new ResourceLocation("speedbuilder:construction0"), null,
				new ItemStack(SpeedBuilderItems.ITEM_CONSTRUCTION0), Ingredient.fromItem(Items.PAPER),
				Ingredient.fromItem(Items.REDSTONE), Ingredient.fromItem(SpeedBuilderItems.ITEM_PENCIL));
		GameRegistry.addShapelessRecipe(new ResourceLocation("speedbuilder:construction0from2"), null,
				new ItemStack(SpeedBuilderItems.ITEM_CONSTRUCTION0),
				Ingredient.fromItem(SpeedBuilderItems.ITEM_CONSTRUCTION2),
				Ingredient.fromItem(SpeedBuilderItems.ITEM_PENCIL));
		GameRegistry.addShapelessRecipe(new ResourceLocation("speedbuilder:construction0from1"), null,
				new ItemStack(SpeedBuilderItems.ITEM_CONSTRUCTION0),
				Ingredient.fromItem(SpeedBuilderItems.ITEM_CONSTRUCTION1),
				Ingredient.fromItem(SpeedBuilderItems.ITEM_PENCIL));
		GameRegistry.addShapelessRecipe(new ResourceLocation("speedbuilder:constructionborder"), null,
				new ItemStack(SpeedBuilderBlocks.CONSTRUCTIONBORDER), Ingredient.fromItem(Items.STICK),
				Ingredient.fromItem(Items.STICK), Ingredient.fromItem(Items.STICK), Ingredient.fromItem(Items.STICK));
	}

	private void registerShapedRecipes() {
		GameRegistry.addShapedRecipe(new ResourceLocation("speedbuilder:mirrordiagonal"), null,
				new ItemStack(SpeedBuilderBlocks.MIRRORDIAGONAL), "C  ", " C ", "  C", 'C',
				SpeedBuilderBlocks.CONSTRUCTIONBORDER);
		GameRegistry.addShapedRecipe(new ResourceLocation("speedbuilder:mirrorhorizontal"), null,
				new ItemStack(SpeedBuilderBlocks.MIRRORHORIZONTAL), " C ", " C ", " C ", 'C',
				SpeedBuilderBlocks.CONSTRUCTIONBORDER);
		GameRegistry.addShapedRecipe(new ResourceLocation("speedbuilder:mirrorhorizontal"), null,
				new ItemStack(SpeedBuilderBlocks.MIRRORVERTICAL), "   ", "CCC", "   ", 'C',
				SpeedBuilderBlocks.CONSTRUCTIONBORDER);
	}
}
